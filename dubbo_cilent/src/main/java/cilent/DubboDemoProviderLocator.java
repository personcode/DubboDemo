package cilent;

import com.alibaba.dubbo.demo.IDemoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by ChangdaChen on 2018/7/20.
 */
public class DubboDemoProviderLocator {

    /**
     * logger
     */
    private final static Logger logger = LoggerFactory.getLogger(DubboDemoProviderLocator.class);

    /**
     * application context file path
     */
    private final static String applicationContextPath = "applicationContext.xml";
    /**
     * application context
     */
    private static ApplicationContext applicationContext = null;

    /**
     * init
     */
    static {
        init();
    }

    public static void init() {
        applicationContext = new ClassPathXmlApplicationContext(applicationContextPath);
    }

    /**
     * get moment service interface
     *
     * @return moment service interface
     */
    public static IDemoService getDemoServiceFacade() {
        boolean status = false;
        IDemoService demoService = null;
        try {
            Object object;
            if ((object = applicationContext.getBean("demoServiceFacade")) != null) {
                demoService = (IDemoService) object;
                status = true;
            }
        } catch (Exception ex) {
            logger.error("get demoServiceFacade interface met error:{}", ex);
        } finally {
            logger.info("get demoServiceFacade interface status:{}", status);
        }
        return demoService;
    }
}
