package com.alibaba.dubbo.demo.impl;

import com.alibaba.dubbo.demo.IDemoService;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by ChangdaChen on 2018/7/19.
 */
@Service("demoServiceFacade")
public class DemoServiceImpl implements IDemoService {


    public List<String> getPermissions(Long id) {
        List<String> demo = new ArrayList<String>();
        demo.add(String.format("Permission_%d",id-1));
        demo.add(String.format("Permission_%d",id));
        demo.add(String.format("Permission_%d",id+1));
        return demo;
    }

    public String printHello(String msg) {
        System.out.println("provider: "+msg);
        return msg;
    }
}
