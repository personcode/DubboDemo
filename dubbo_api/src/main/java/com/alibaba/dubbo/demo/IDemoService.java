package com.alibaba.dubbo.demo;

import java.util.List;

/**
 * Created by ChangdaChen on 2018/7/19.
 */
public interface IDemoService {
    List<String> getPermissions(Long id);
    String printHello(String msg);
}
