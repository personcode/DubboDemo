package com.alibaba.dubbo.external;

import cilent.DubboDemoProviderLocator;
import com.alibaba.dubbo.demo.IDemoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by ChangdaChen on 2018/7/20.
 */
public class DemoServiceExternal {
    private static final Logger logger = LoggerFactory.getLogger(DemoServiceExternal.class);
    /**
     * get mobile service interface
     */
    private volatile static IDemoService demoServiceFacade = null;

    public static IDemoService getDemoServiceFacade() {
        try {
            if (demoServiceFacade == null) {
                synchronized (DemoServiceExternal.class) {
                    IDemoService temp = demoServiceFacade;
                    if (temp == null) {
                        temp = DubboDemoProviderLocator.getDemoServiceFacade();
                        if (temp != null) {
                            demoServiceFacade = temp;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            demoServiceFacade = null;
            logger.error("get demoServiceFacade met error, error:{}", ex);
        } finally {
//            logger.debug("get demoServiceFacade, status:{}", demoServiceFacade != null);
        }
        return demoServiceFacade;
    }
}
