package com.alibaba.dubbo.consumer;

import com.alibaba.dubbo.demo.IDemoService;
import com.alibaba.dubbo.external.DemoServiceExternal;

/**
 * Created by wy on 2017/4/13.
 */
public class Consumer {
    public static void main(String[] args) {

//        //测试常规服务
//        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("consumer.xml");
//        context.start();
//        System.out.println("consumer2 start");
//        DemoService demoService = context.getBean(DemoService.class);
//        System.out.println("consumer2");
//        System.out.println(demoService.getPermissions(1L));
        System.out.println("consumer1");
        IDemoService demoService = DemoServiceExternal.getDemoServiceFacade();
        System.out.println(demoService.getPermissions(5L));
        demoService.printHello("consumer1");
        System.out.println("consumer1: "+ demoService.printHello("consumer1"));

    }
}
