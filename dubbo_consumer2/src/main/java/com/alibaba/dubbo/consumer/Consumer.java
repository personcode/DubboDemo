package com.alibaba.dubbo.consumer;

import com.alibaba.dubbo.demo.IDemoService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by wy on 2017/4/13.
 */
public class Consumer {
    public static void main(String[] args) {

        //测试常规服务
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("consumer.xml");
        context.start();
        System.out.println("consumer2 start");
        IDemoService demoService = (IDemoService) context.getBean("demoServiceFacade");
        System.out.println("consumer2");
        System.out.println(demoService.getPermissions(1L));
        System.out.println("consumer2: "+demoService.printHello("consumer2"));
        while(true){
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("consumer2: "+ demoService.printHello("consumer2"));
        }

    }
}
